EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_SPST SW1
U 1 1 5FDEC5BB
P 3700 2350
F 0 "SW1" H 3700 2585 50  0000 C CNN
F 1 "SW_SPST" H 3700 2494 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 3700 2350 50  0001 C CNN
F 3 "~" H 3700 2350 50  0001 C CNN
	1    3700 2350
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW2
U 1 1 5FDEC7CF
P 3700 2700
F 0 "SW2" H 3700 2935 50  0000 C CNN
F 1 "SW_SPST" H 3700 2844 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 3700 2700 50  0001 C CNN
F 3 "~" H 3700 2700 50  0001 C CNN
	1    3700 2700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW3
U 1 1 5FDECA88
P 3700 3050
F 0 "SW3" H 3700 3285 50  0000 C CNN
F 1 "SW_SPST" H 3700 3194 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 3700 3050 50  0001 C CNN
F 3 "~" H 3700 3050 50  0001 C CNN
	1    3700 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5FDED392
P 4000 3150
F 0 "#PWR01" H 4000 2900 50  0001 C CNN
F 1 "GND" H 4005 2977 50  0000 C CNN
F 2 "" H 4000 3150 50  0001 C CNN
F 3 "" H 4000 3150 50  0001 C CNN
	1    4000 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 5FDED629
P 4350 2700
F 0 "D2" H 4343 2445 50  0000 C CNN
F 1 "LED" H 4343 2536 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4350 2700 50  0001 C CNN
F 3 "~" H 4350 2700 50  0001 C CNN
	1    4350 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3150 4000 3050
Wire Wire Line
	4000 2350 3900 2350
Wire Wire Line
	3900 2700 4000 2700
Connection ~ 4000 2700
Wire Wire Line
	4000 2700 4000 2350
Wire Wire Line
	3900 3050 4000 3050
Connection ~ 4000 3050
Wire Wire Line
	4000 3050 4000 2700
$Comp
L Device:LED D1
U 1 1 5FDF0052
P 4350 2350
F 0 "D1" H 4343 2095 50  0000 C CNN
F 1 "LED" H 4343 2186 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4350 2350 50  0001 C CNN
F 3 "~" H 4350 2350 50  0001 C CNN
	1    4350 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2350 4000 2350
Connection ~ 4000 2350
Wire Wire Line
	4200 2700 4000 2700
Text Label 2650 3200 0    50   ~ 0
pb3
Text Label 2150 3200 2    50   ~ 0
led1
Text Label 2150 3100 2    50   ~ 0
led2
Text Label 3500 2350 2    50   ~ 0
pb1
Text Label 3500 3050 2    50   ~ 0
pb3
Text Label 4900 2350 0    50   ~ 0
led1
Text Label 4900 2700 0    50   ~ 0
led2
Text Label 3500 2700 2    50   ~ 0
pb2
$Comp
L Device:R_Small R2
U 1 1 5FE02945
P 4800 2700
F 0 "R2" V 4604 2700 50  0000 C CNN
F 1 "140" V 4695 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 4800 2700 50  0001 C CNN
F 3 "~" H 4800 2700 50  0001 C CNN
	1    4800 2700
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5FE02BB2
P 4800 2350
F 0 "R1" V 4604 2350 50  0000 C CNN
F 1 "140" V 4695 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 4800 2350 50  0001 C CNN
F 3 "~" H 4800 2350 50  0001 C CNN
	1    4800 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 2700 4500 2700
Wire Wire Line
	4500 2350 4700 2350
Text Label 2650 3300 0    50   ~ 0
pb2
Text Label 2650 3400 0    50   ~ 0
pb1
$Comp
L power:GND #PWR02
U 1 1 5FE1BB8D
P 2650 3100
F 0 "#PWR02" H 2650 2850 50  0001 C CNN
F 1 "GND" H 2655 2927 50  0000 C CNN
F 2 "" H 2650 3100 50  0001 C CNN
F 3 "" H 2650 3100 50  0001 C CNN
	1    2650 3100
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 5FE796E9
P 5900 2600
F 0 "J2" H 5928 2576 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5928 2485 50  0000 L CNN
F 2 "Connector_JST:JST_PH_S2B-PH-K_1x02_P2.00mm_Horizontal" H 5900 2600 50  0001 C CNN
F 3 "~" H 5900 2600 50  0001 C CNN
	1    5900 2600
	1    0    0    -1  
$EndComp
Text Notes 5600 2300 0    50   ~ 0
battery conn
$Comp
L power:GND #PWR0101
U 1 1 5FE844C9
P 6050 7200
F 0 "#PWR0101" H 6050 6950 50  0001 C CNN
F 1 "GND" H 6055 7027 50  0000 C CNN
F 2 "" H 6050 7200 50  0001 C CNN
F 3 "" H 6050 7200 50  0001 C CNN
	1    6050 7200
	1    0    0    -1  
$EndComp
Text Label 6050 6900 0    50   ~ 0
mic_data
Text Label 2150 3000 2    50   ~ 0
mic_data
$Comp
L power:+3V3 #PWR0103
U 1 1 5FE905C9
P 2150 2900
F 0 "#PWR0103" H 2150 2750 50  0001 C CNN
F 1 "+3V3" V 2165 3028 50  0000 L CNN
F 2 "" H 2150 2900 50  0001 C CNN
F 3 "" H 2150 2900 50  0001 C CNN
	1    2150 2900
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5FE9245B
P 5700 2700
F 0 "#PWR0104" H 5700 2450 50  0001 C CNN
F 1 "GND" H 5705 2527 50  0000 C CNN
F 2 "" H 5700 2700 50  0001 C CNN
F 3 "" H 5700 2700 50  0001 C CNN
	1    5700 2700
	1    0    0    -1  
$EndComp
$Comp
L TPS613222ADBVR:TPS613222ADBVR U2
U 1 1 5FEAD271
P 9200 2550
F 0 "U2" H 9200 2920 50  0000 C CNN
F 1 "TPS613222ADBVR" H 9200 2829 50  0000 C CNN
F 2 "TPS613222ADBVR:SOT95P280X145-5N" H 9200 2550 50  0001 L BNN
F 3 "" H 9200 2550 50  0001 L BNN
	1    9200 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 5FEAE0C2
P 8250 2450
F 0 "L1" V 8440 2450 50  0000 C CNN
F 1 "2.2uH" V 8349 2450 50  0000 C CNN
F 2 "MAMK2520H2R2M:INDC2520X120N" H 8250 2450 50  0001 C CNN
F 3 "~" H 8250 2450 50  0001 C CNN
	1    8250 2450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5FEAE515
P 8900 1550
F 0 "R3" V 8693 1550 50  0000 C CNN
F 1 "5" V 8784 1550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8830 1550 50  0001 C CNN
F 3 "~" H 8900 1550 50  0001 C CNN
	1    8900 1550
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5FEAE7EB
P 9800 1550
F 0 "C1" V 9548 1550 50  0000 C CNN
F 1 "330pF" V 9639 1550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9838 1400 50  0001 C CNN
F 3 "~" H 9800 1550 50  0001 C CNN
	1    9800 1550
	0    1    1    0   
$EndComp
$Comp
L Device:D_Schottky D3
U 1 1 5FEAEEBA
P 9200 1950
F 0 "D3" H 9200 1734 50  0000 C CNN
F 1 "D_Schottky" H 9200 1825 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 9200 1950 50  0001 C CNN
F 3 "~" H 9200 1950 50  0001 C CNN
	1    9200 1950
	-1   0    0    1   
$EndComp
$Comp
L Device:C C2
U 1 1 5FEAF0B3
P 10250 2600
F 0 "C2" H 10365 2646 50  0000 L CNN
F 1 "22uF" H 10365 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10288 2450 50  0001 C CNN
F 3 "~" H 10250 2600 50  0001 C CNN
	1    10250 2600
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR0106
U 1 1 5FEAFF9D
P 5700 2600
F 0 "#PWR0106" H 5700 2450 50  0001 C CNN
F 1 "+BATT" H 5715 2773 50  0000 C CNN
F 2 "" H 5700 2600 50  0001 C CNN
F 3 "" H 5700 2600 50  0001 C CNN
	1    5700 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 2450 8450 2450
Wire Wire Line
	8450 2450 8450 1950
Wire Wire Line
	8450 1550 8750 1550
Connection ~ 8450 2450
Wire Wire Line
	8450 2450 8500 2450
Wire Wire Line
	8450 1950 9050 1950
Connection ~ 8450 1950
Wire Wire Line
	8450 1950 8450 1550
Wire Wire Line
	9050 1550 9650 1550
Wire Wire Line
	9350 1950 10250 1950
Wire Wire Line
	10250 1950 10250 2450
Wire Wire Line
	10250 2450 9900 2450
Wire Wire Line
	9950 1550 10250 1550
Wire Wire Line
	10250 1550 10250 1950
Connection ~ 10250 1950
Connection ~ 10250 2450
$Comp
L power:GND #PWR0107
U 1 1 5FEBABC4
P 9900 2650
F 0 "#PWR0107" H 9900 2400 50  0001 C CNN
F 1 "GND" H 9905 2477 50  0000 C CNN
F 2 "" H 9900 2650 50  0001 C CNN
F 3 "" H 9900 2650 50  0001 C CNN
	1    9900 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5FEBAE00
P 10250 2750
F 0 "#PWR0108" H 10250 2500 50  0001 C CNN
F 1 "GND" H 10255 2577 50  0000 C CNN
F 2 "" H 10250 2750 50  0001 C CNN
F 3 "" H 10250 2750 50  0001 C CNN
	1    10250 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 2450 10450 2450
$Comp
L power:+5V #PWR0109
U 1 1 5FEBEF93
P 10450 2450
F 0 "#PWR0109" H 10450 2300 50  0001 C CNN
F 1 "+5V" V 10465 2578 50  0000 L CNN
F 2 "" H 10450 2450 50  0001 C CNN
F 3 "" H 10450 2450 50  0001 C CNN
	1    10450 2450
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0110
U 1 1 5FED0A23
P 2650 2900
F 0 "#PWR0110" H 2650 2750 50  0001 C CNN
F 1 "+5V" V 2665 3028 50  0000 L CNN
F 2 "" H 2650 2900 50  0001 C CNN
F 3 "" H 2650 2900 50  0001 C CNN
	1    2650 2900
	0    1    1    0   
$EndComp
Text Notes 7800 1300 0    50   ~ 0
component math has been verified\nglue components selected
$Comp
L Battery_Management:MCP73811T-420I-OT U3
U 1 1 5FED78A1
P 9400 5150
F 0 "U3" H 9600 5400 50  0000 L CNN
F 1 "MCP73811T-420I-OT" H 9000 5650 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 9450 4900 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/22036b.pdf" H 9150 5400 50  0001 C CNN
	1    9400 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5FED8269
P 8550 4950
F 0 "C3" H 8665 4996 50  0000 L CNN
F 1 "22uF" H 8665 4905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8588 4800 50  0001 C CNN
F 3 "~" H 8550 4950 50  0001 C CNN
	1    8550 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5FEDA579
P 10000 5200
F 0 "C4" H 10115 5246 50  0000 L CNN
F 1 "22uF" H 10115 5155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10038 5050 50  0001 C CNN
F 3 "~" H 10000 5200 50  0001 C CNN
	1    10000 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 5050 8950 5250
Wire Wire Line
	8950 5250 9000 5250
Wire Wire Line
	9000 5050 8950 5050
Wire Wire Line
	8950 5050 8950 4800
Wire Wire Line
	8950 4800 9400 4800
Wire Wire Line
	9400 4800 9400 4850
Connection ~ 8950 5050
Wire Wire Line
	8950 4800 8550 4800
Connection ~ 8950 4800
Connection ~ 8550 4800
$Comp
L power:GND #PWR0111
U 1 1 5FEDCEB0
P 8550 5450
F 0 "#PWR0111" H 8550 5200 50  0001 C CNN
F 1 "GND" H 8555 5277 50  0000 C CNN
F 2 "" H 8550 5450 50  0001 C CNN
F 3 "" H 8550 5450 50  0001 C CNN
	1    8550 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5FEDE02A
P 9400 5450
F 0 "#PWR0112" H 9400 5200 50  0001 C CNN
F 1 "GND" H 9405 5277 50  0000 C CNN
F 2 "" H 9400 5450 50  0001 C CNN
F 3 "" H 9400 5450 50  0001 C CNN
	1    9400 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 5100 8550 5450
Wire Wire Line
	9800 5050 10000 5050
$Comp
L power:+BATT #PWR0113
U 1 1 5FEE06CD
P 10000 5050
F 0 "#PWR0113" H 10000 4900 50  0001 C CNN
F 1 "+BATT" H 10015 5223 50  0000 C CNN
F 2 "" H 10000 5050 50  0001 C CNN
F 3 "" H 10000 5050 50  0001 C CNN
	1    10000 5050
	1    0    0    -1  
$EndComp
Connection ~ 10000 5050
$Comp
L power:GND #PWR0114
U 1 1 5FEE2837
P 10000 5450
F 0 "#PWR0114" H 10000 5200 50  0001 C CNN
F 1 "GND" H 10005 5277 50  0000 C CNN
F 2 "" H 10000 5450 50  0001 C CNN
F 3 "" H 10000 5450 50  0001 C CNN
	1    10000 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 5350 10000 5450
Text Notes 8450 6050 0    50   ~ 0
rpi onboard mUSB used to charge battery ...\nsince connected to +5V rail
Text Notes 8300 3350 0    50   ~ 0
figure out how current should flow for following states\nbattery being charged\nbattery powering rpi\nbattery being charged but also powering rpi
Text Notes 4400 1750 0    50   ~ 0
verify whether there is voltage cut-off to protect battery ...\nfrom going too low
$Comp
L Connector:Raspberry_Pi_2_3 J3
U 1 1 5FEF329E
P 2400 5700
F 0 "J3" H 2400 7181 50  0000 C CNN
F 1 "Raspberry_Pi_2_3" H 2400 7090 50  0000 C CNN
F 2 "Module:Raspberry_Pi_Zero_Socketed_THT_FaceDown_MountingHoles" H 2400 5700 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 2400 5700 50  0001 C CNN
	1    2400 5700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0116
U 1 1 5FF10E6E
P 2650 3000
F 0 "#PWR0116" H 2650 2850 50  0001 C CNN
F 1 "+5V" V 2665 3128 50  0000 L CNN
F 2 "" H 2650 3000 50  0001 C CNN
F 3 "" H 2650 3000 50  0001 C CNN
	1    2650 3000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5FF1ED09
P 2150 3300
F 0 "#PWR0117" H 2150 3050 50  0001 C CNN
F 1 "GND" H 2155 3127 50  0000 C CNN
F 2 "" H 2150 3300 50  0001 C CNN
F 3 "" H 2150 3300 50  0001 C CNN
	1    2150 3300
	0    1    1    0   
$EndComp
$Comp
L SPH0641LM4H-1:SPH0641LM4H-1 MK1
U 1 1 5FF27FAA
P 5350 6900
F 0 "MK1" H 5350 7367 50  0000 C CNN
F 1 "SPH0641LM4H-1" H 5350 7276 50  0000 C CNN
F 2 "SPH0641LM4H-1:MIC_SPH0641LM4H-1" H 5350 6900 50  0001 L BNN
F 3 "" H 5350 6900 50  0001 L BNN
F 4 "D" H 5350 6900 50  0001 L BNN "PARTREV"
F 5 "Knowles" H 5350 6900 50  0001 L BNN "MANUFACTURER"
F 6 "1.08 mm" H 5350 6900 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 7 "Manufacturer Recommendations" H 5350 6900 50  0001 L BNN "STANDARD"
	1    5350 6900
	1    0    0    -1  
$EndComp
Text Label 6050 6700 0    50   ~ 0
mic_3v3
$Comp
L power:GND #PWR03
U 1 1 5FF4B647
P 4250 6900
F 0 "#PWR03" H 4250 6650 50  0001 C CNN
F 1 "GND" H 4255 6727 50  0000 C CNN
F 2 "" H 4250 6900 50  0001 C CNN
F 3 "" H 4250 6900 50  0001 C CNN
	1    4250 6900
	0    1    1    0   
$EndComp
Text Label 4650 7000 2    50   ~ 0
mic_clk
Wire Wire Line
	4250 6900 4650 6900
Text Label 2150 3500 2    50   ~ 0
mic_clk
Text Notes 8050 1050 0    50   ~ 0
Boost DC/DC
Wire Notes Line
	7700 1100 7700 3450
Wire Notes Line
	10750 3450 10750 1100
Text Notes 8200 4300 0    50   ~ 0
PMIC - Lion charger
Wire Notes Line
	8200 4400 8200 6150
Wire Notes Line
	8200 6150 10650 6150
Wire Notes Line
	10650 6150 10650 4400
Wire Notes Line
	10650 4400 8200 4400
Text Notes 3950 6100 0    50   ~ 0
Digital Mic
Wire Notes Line
	3900 6150 3900 7600
Wire Notes Line
	3900 7600 6650 7600
Wire Notes Line
	6650 7600 6650 6150
Wire Notes Line
	6650 6150 3900 6150
Text Notes 3350 1850 0    50   ~ 0
HMI
Wire Notes Line
	3300 1900 3300 3450
Wire Notes Line
	3300 3450 5200 3450
Wire Notes Line
	5200 3450 5200 1900
Wire Notes Line
	5200 1900 3300 1900
Text Notes 1750 2450 0    50   ~ 0
RPI interface
Text Notes 1150 1950 3    50   ~ 0
mic draws <1mA, 3v3 logic level\nrpi GPIO should be able to handle\nGPIO gives control over on/off mic\nhttps://raspberrypi.stackexchange.com/questions/3209/what-are-the-min-max-voltage-current-values-the-gpio-pins-can-handle
Wire Wire Line
	8550 4800 8350 4800
$Comp
L power:+5V #PWR06
U 1 1 5FF99978
P 8350 4800
F 0 "#PWR06" H 8350 4650 50  0001 C CNN
F 1 "+5V" H 8365 4973 50  0000 C CNN
F 2 "" H 8350 4800 50  0001 C CNN
F 3 "" H 8350 4800 50  0001 C CNN
	1    8350 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR05
U 1 1 5FFB4A4F
P 8100 2450
F 0 "#PWR05" H 8100 2300 50  0001 C CNN
F 1 "+BATT" V 8115 2577 50  0000 L CNN
F 2 "" H 8100 2450 50  0001 C CNN
F 3 "" H 8100 2450 50  0001 C CNN
	1    8100 2450
	0    -1   -1   0   
$EndComp
Wire Notes Line
	7700 3450 10750 3450
Wire Notes Line
	7700 1100 10750 1100
Text Label 2150 3400 2    50   ~ 0
mic_3v3
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J1
U 1 1 5FFD101A
P 2350 3200
F 0 "J1" H 2400 3717 50  0000 C CNN
F 1 "Conn_02x07_Odd_Even" H 2400 3626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x07_P2.54mm_Vertical" H 2350 3200 50  0001 C CNN
F 3 "~" H 2350 3200 50  0001 C CNN
	1    2350 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5FFD4595
P 2650 3500
F 0 "#PWR04" H 2650 3250 50  0001 C CNN
F 1 "GND" H 2655 3327 50  0000 C CNN
F 2 "" H 2650 3500 50  0001 C CNN
F 3 "" H 2650 3500 50  0001 C CNN
	1    2650 3500
	0    -1   -1   0   
$EndComp
Text Notes 850  1000 0    50   ~ 0
power estimations\nhttps://www.raspberrypi.org/documentation/hardware/raspberrypi/power/README.md\nhttps://cdn.sparkfun.com/datasheets/Sensors/LightImaging/OV5640_datasheet.pdf
$Comp
L Power_Supervisor:LM809 U1
U 1 1 6002E8C4
P 5400 4550
F 0 "U1" H 5170 4596 50  0000 R CNN
F 1 "LM809" H 5170 4505 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5700 4650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm809.pdf" H 5700 4650 50  0001 C CNN
	1    5400 4550
	1    0    0    -1  
$EndComp
Text Notes 4450 5800 0    50   ~ 0
actually the MIC809, but couldn't find symbol
Text Notes 4600 4000 0    50   ~ 0
Power switch supervisor
$Comp
L power:GND #PWR09
U 1 1 60037D53
P 5400 5050
F 0 "#PWR09" H 5400 4800 50  0001 C CNN
F 1 "GND" H 5405 4877 50  0000 C CNN
F 2 "" H 5400 5050 50  0001 C CNN
F 3 "" H 5400 5050 50  0001 C CNN
	1    5400 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 4950 5400 5000
Connection ~ 5400 5000
Wire Wire Line
	5400 5000 5400 5050
$Comp
L Device:R R4
U 1 1 6003C638
P 4900 5000
F 0 "R4" V 4693 5000 50  0000 C CNN
F 1 "100k" V 4784 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4830 5000 50  0001 C CNN
F 3 "~" H 4900 5000 50  0001 C CNN
	1    4900 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 5000 5400 5000
Wire Wire Line
	5400 4150 4750 4150
Wire Wire Line
	4750 4150 4750 5000
Wire Wire Line
	4750 4150 4500 4150
Connection ~ 4750 4150
$Comp
L power:+5V #PWR07
U 1 1 600410E0
P 4500 4150
F 0 "#PWR07" H 4500 4000 50  0001 C CNN
F 1 "+5V" V 4515 4278 50  0000 L CNN
F 2 "" H 4500 4150 50  0001 C CNN
F 3 "" H 4500 4150 50  0001 C CNN
	1    4500 4150
	0    -1   -1   0   
$EndComp
Text Notes 4450 5900 0    50   ~ 0
PMOS sym is placeholder, verify w/ actual part
Wire Wire Line
	5700 4550 5950 4550
Wire Wire Line
	5950 4550 5950 5100
Wire Wire Line
	6450 5400 6150 5400
Wire Wire Line
	6450 4800 6750 4800
Wire Wire Line
	6450 4800 6450 5400
Wire Wire Line
	5750 5400 4500 5400
$Comp
L power:+BATT #PWR08
U 1 1 600684E0
P 4500 5400
F 0 "#PWR08" H 4500 5250 50  0001 C CNN
F 1 "+BATT" V 4515 5527 50  0000 L CNN
F 2 "" H 4500 5400 50  0001 C CNN
F 3 "" H 4500 5400 50  0001 C CNN
	1    4500 5400
	0    -1   -1   0   
$EndComp
Text Notes 4150 5950 0    50   ~ 0
https://www.maximintegrated.com/en/design/technical-documents/app-notes/1/1136.html
Text Notes 3550 1450 0    50   ~ 0
may need to make distinction between +5V rail and ...\nexternal +5V prior to protection
$Comp
L Device:Q_PMOS_GSD Q1
U 1 1 6007B7E2
P 5950 5300
F 0 "Q1" V 6199 5300 50  0000 C CNN
F 1 "Q_PMOS_GSD" V 6290 5300 50  0000 C CNN
F 2 "" H 6150 5400 50  0001 C CNN
F 3 "~" H 5950 5300 50  0001 C CNN
	1    5950 5300
	0    -1   1    0   
$EndComp
Text Notes 6700 5400 0    50   ~ 0
drain to batt\nsource to load\n(was flipped over y)
$Comp
L power:+VDC #PWR?
U 1 1 600CFD91
P 4200 5200
F 0 "#PWR?" H 4200 5100 50  0001 C CNN
F 1 "+VDC" H 4200 5475 50  0000 C CNN
F 2 "" H 4200 5200 50  0001 C CNN
F 3 "" H 4200 5200 50  0001 C CNN
	1    4200 5200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
