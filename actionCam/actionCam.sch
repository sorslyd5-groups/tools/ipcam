EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_SPST SW1
U 1 1 5FDEC5BB
P 3700 2350
F 0 "SW1" H 3700 2585 50  0000 C CNN
F 1 "SW_SPST" H 3700 2494 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 3700 2350 50  0001 C CNN
F 3 "~" H 3700 2350 50  0001 C CNN
	1    3700 2350
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW2
U 1 1 5FDEC7CF
P 3700 2700
F 0 "SW2" H 3700 2935 50  0000 C CNN
F 1 "SW_SPST" H 3700 2844 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 3700 2700 50  0001 C CNN
F 3 "~" H 3700 2700 50  0001 C CNN
	1    3700 2700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW3
U 1 1 5FDECA88
P 3700 3050
F 0 "SW3" H 3700 3285 50  0000 C CNN
F 1 "SW_SPST" H 3700 3194 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 3700 3050 50  0001 C CNN
F 3 "~" H 3700 3050 50  0001 C CNN
	1    3700 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5FDED392
P 4000 3150
F 0 "#PWR01" H 4000 2900 50  0001 C CNN
F 1 "GND" H 4005 2977 50  0000 C CNN
F 2 "" H 4000 3150 50  0001 C CNN
F 3 "" H 4000 3150 50  0001 C CNN
	1    4000 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 5FDED629
P 4350 2700
F 0 "D2" H 4343 2445 50  0000 C CNN
F 1 "LED" H 4343 2536 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4350 2700 50  0001 C CNN
F 3 "~" H 4350 2700 50  0001 C CNN
	1    4350 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3150 4000 3050
Wire Wire Line
	4000 2350 3900 2350
Wire Wire Line
	3900 2700 4000 2700
Connection ~ 4000 2700
Wire Wire Line
	4000 2700 4000 2350
Wire Wire Line
	3900 3050 4000 3050
Connection ~ 4000 3050
Wire Wire Line
	4000 3050 4000 2700
$Comp
L Device:LED D1
U 1 1 5FDF0052
P 4350 2350
F 0 "D1" H 4343 2095 50  0000 C CNN
F 1 "LED" H 4343 2186 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4350 2350 50  0001 C CNN
F 3 "~" H 4350 2350 50  0001 C CNN
	1    4350 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2350 4000 2350
Connection ~ 4000 2350
Wire Wire Line
	4200 2700 4000 2700
Text Label 2150 3000 2    50   ~ 0
pb3
Text Label 2150 3200 2    50   ~ 0
led1
Text Label 2150 3100 2    50   ~ 0
led2
Text Label 3500 2350 2    50   ~ 0
pb1
Text Label 3500 3050 2    50   ~ 0
pb3
Text Label 4900 2350 0    50   ~ 0
led1
Text Label 4900 2700 0    50   ~ 0
led2
Text Label 3500 2700 2    50   ~ 0
pb2
$Comp
L Device:R_Small R2
U 1 1 5FE02945
P 4800 2700
F 0 "R2" V 4604 2700 50  0000 C CNN
F 1 "55" V 4695 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4800 2700 50  0001 C CNN
F 3 "~" H 4800 2700 50  0001 C CNN
	1    4800 2700
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5FE02BB2
P 4800 2350
F 0 "R1" V 4604 2350 50  0000 C CNN
F 1 "55" V 4695 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4800 2350 50  0001 C CNN
F 3 "~" H 4800 2350 50  0001 C CNN
	1    4800 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 2700 4500 2700
Wire Wire Line
	4500 2350 4700 2350
Text Label 2650 3200 0    50   ~ 0
pb2
Text Label 2650 3300 0    50   ~ 0
pb1
$Comp
L power:GND #PWR02
U 1 1 5FE1BB8D
P 2650 3100
F 0 "#PWR02" H 2650 2850 50  0001 C CNN
F 1 "GND" H 2655 2927 50  0000 C CNN
F 2 "" H 2650 3100 50  0001 C CNN
F 3 "" H 2650 3100 50  0001 C CNN
	1    2650 3100
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0103
U 1 1 5FE905C9
P 2150 2900
F 0 "#PWR0103" H 2150 2750 50  0001 C CNN
F 1 "+3V3" V 2165 3028 50  0000 L CNN
F 2 "" H 2150 2900 50  0001 C CNN
F 3 "" H 2150 2900 50  0001 C CNN
	1    2150 2900
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0110
U 1 1 5FED0A23
P 2650 2900
F 0 "#PWR0110" H 2650 2750 50  0001 C CNN
F 1 "+5V" V 2665 3028 50  0000 L CNN
F 2 "" H 2650 2900 50  0001 C CNN
F 3 "" H 2650 2900 50  0001 C CNN
	1    2650 2900
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0116
U 1 1 5FF10E6E
P 2650 3000
F 0 "#PWR0116" H 2650 2850 50  0001 C CNN
F 1 "+5V" V 2665 3128 50  0000 L CNN
F 2 "" H 2650 3000 50  0001 C CNN
F 3 "" H 2650 3000 50  0001 C CNN
	1    2650 3000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5FF1ED09
P 2150 3300
F 0 "#PWR0117" H 2150 3050 50  0001 C CNN
F 1 "GND" H 2155 3127 50  0000 C CNN
F 2 "" H 2150 3300 50  0001 C CNN
F 3 "" H 2150 3300 50  0001 C CNN
	1    2150 3300
	0    1    1    0   
$EndComp
Text Notes 3350 1850 0    50   ~ 0
HMI
Wire Notes Line
	3300 1900 3300 3450
Wire Notes Line
	3300 3450 5200 3450
Wire Notes Line
	5200 3450 5200 1900
Wire Notes Line
	5200 1900 3300 1900
Text Notes 1750 2450 0    50   ~ 0
RPI interface
Text Notes 1150 1950 3    50   ~ 0
mic draws <1mA, 3v3 logic level\nrpi GPIO should be able to handle\nGPIO gives control over on/off mic\nhttps://raspberrypi.stackexchange.com/questions/3209/what-are-the-min-max-voltage-current-values-the-gpio-pins-can-handle
Text Notes 850  1000 0    50   ~ 0
power estimations\nhttps://www.raspberrypi.org/documentation/hardware/raspberrypi/power/README.md\nhttps://cdn.sparkfun.com/datasheets/Sensors/LightImaging/OV5640_datasheet.pdf
Text Notes 3200 3800 0    50   ~ 0
serial to parallel for PDM (3MHz, rpi gpio won't be able to handle)\nfclock wants 2.4MHz to 4.8MHz - industry standard 3.072MHz\n24KHz\ndedicated clock line\n
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5FFAE517
P 2350 3100
F 0 "J1" H 2400 3517 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 2400 3426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 2350 3100 50  0001 C CNN
F 3 "~" H 2350 3100 50  0001 C CNN
	1    2350 3100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
