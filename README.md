# meta

project goals:
- cheap and scalable
- understand operation
- explore new technologies

exploratory areas:
- uninterruptible power supply (UPS) circuit
- rpi hat
- MEMS mic
- Li-ion battery protections

# changelog
- v1: rpi-based hat, PDM mic, no battery
- v2: esp32-based, battery w/ UPS circuit
